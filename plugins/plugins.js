import Vue from 'vue'
import VModal from 'vue-js-modal'
import Vuex from 'vuex'
import VShowSlide from 'v-show-slide'

Vue.use(VModal)
Vue.use(Vuex)
Vue.use(VShowSlide, {
  customEasing: {
    exampleEasing: 'ease'
  }
})
